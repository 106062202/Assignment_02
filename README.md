# Raiden

## Jucify Mechanisms

### Level
* Level 1: Enemies generate at 1 sec per enemy.
* Level 2: Enemy generation rate increased to 400 ms per enemy.
* Level 3: Enemy generation rate increased to 200 ms per enemy, and enemies track the player.

### Skill
* Cooldown: The ultimate skill can be used every 5 seconds.
* Thanos snap: Half of the enemies destroyed when the skill used.
* Icon: A star icon will appear in the status bar.

## Animations
* When the player uses the ultimate skill: Implemented by tweens, the animation scales the player.

## Particle Systems
* When an enemy explodes or the player dies, a particle system animation is triggered.

## Sound Effects
* Theme song: The intro music of the actual Raiden game.
* Explosion: When an enemy explodes, there is an explosion sound effect.
* Firing: When the player fires, there is a firing sound effect.
