const config = {
    type: Phaser.AUTO,
    width: 640,
    height: 480,
    backgroundColor: "black",
    physics: {
        default: "arcade",
        arcade: {
            gravity: { x: 0, y: 0 }
        }
    },
    scene: [
        SceneMainMenu,
        SceneMain,
        SceneGameOver,
        ScenePause
    ],
    pixelArt: true,
    roundPixels: true
};

const game = new Phaser.Game(config);
