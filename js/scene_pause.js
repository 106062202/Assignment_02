class ScenePause extends Phaser.Scene {
    constructor() {
        super({ key: 'ScenePause' });
    }

    preload() {
        this.load.image('sprBtnRes', 'assets/resume.png');
    }

    create() {
        // Title
        this.title = this.add.text(this.game.config.width * 0.5, 128, 'PAUSED', {
            fontFamily: 'Arial',
            fontSize: 65,
            fontStyle: 'bold',
            color: '#f4d942',
            align: 'center'
        });

        this.title.setOrigin(0.5);


        // Resume Button
        this.btnRes = this.add.sprite(
            this.game.config.width * 0.5,
            this.game.config.height * 0.6,
            'sprBtnRes'
        );

        this.btnRes.setInteractive();

        this.btnRes.on('pointerdown', function() {
            this.btnRes.setTexture('sprBtnRes');
            this.scene.sleep();
            this.scene.resume('SceneMain');
        }, this);
    }

    update() {
    }
}
