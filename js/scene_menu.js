class SceneMainMenu extends Phaser.Scene {
    constructor() {
      super({ key: 'SceneMainMenu' });
    }

    preload() {
        this.load.image('sprBtnPlay', 'assets/sprBtnPlay.png')
        this.load.audio('themeSong', 'assets/theme.ogg');
    }

    create() {
        // Title
        this.title = this.add.text(this.game.config.width * 0.5, 128, 'RAIDEN', {
            fontFamily: 'Arial',
            fontSize: 65,
            fontStyle: 'bold',
            color: '#f4d942',
            align: 'center'
        });

        this.title.setOrigin(0.5);


        // Play Button
        this.btnPlay = this.add.sprite(
            this.game.config.width * 0.5,
            this.game.config.height * 0.6,
            'sprBtnPlay'
        );

        this.btnPlay.setInteractive();

        this.btnPlay.on('pointerup', () => {
            this.btnPlay.setTexture('sprBtnPlay');
            this.scene.start('SceneMain');
        }, this);

        // Theme song
        this.themeSong = this.sound.add('themeSong');
        this.themeSong.play({
            loop: true
        })

        // Volume control
        const cntrl = new dat.GUI({name: 'Volume Control'});

        cntrl.add(this.sound, 'volume', 0, 1).listen();
        cntrl.show();
    }

    update() {
    }
}
