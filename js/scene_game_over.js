class SceneGameOver extends Phaser.Scene {
    constructor() {
        super({ key: 'SceneGameOver' });
    }

    preload() {
        this.load.image('sprBtnRestart', 'assets/sprBtnRestart.png')
    }

    create() {
        // Title
        this.title = this.add.text(this.game.config.width * 0.5, 128, 'GAME OVER', {
            fontFamily: 'Arial',
            fontSize: 65,
            fontStyle: 'bold',
            color: '#f4d942',
            align: 'center'
        });
        this.title.setOrigin(0.5);

        // Restart Button
        this.btnPlay = this.add.sprite(
            this.game.config.width * 0.5,
            this.game.config.height * 0.6,
            'sprBtnRestart'
        );

        this.btnPlay.setInteractive();

        this.btnPlay.on('pointerup', () => {
            this.btnPlay.setTexture('sprBtnRestart');
            this.scene.start('SceneMain');
        }, this);
    }

    update() {
    }
}
