class SceneMain extends Phaser.Scene {
    constructor() {
        super({ key: 'SceneMain' });
    }

    preload() {
        // Spritesheets
        this.load.spritesheet('sprPlayer', 'assets/sprPlayer.png', {
            frameWidth: 16,
            frameHeight: 16
        });
        this.load.spritesheet('sprEnemy', 'assets/sprEnemy.png', {
            frameWidth: 16,
            frameHeight: 16
        });
        this.load.spritesheet('sprExplosion', 'assets/sprExplosion.png', {
            frameWidth: 32,
            frameHeight: 32
        });

        // Images
        this.load.image('sprLaserPlayer', "assets/sprLaserPlayer.png");
        this.load.image('sprLaserEnemy', "assets/sprLaserEnemy.png");
        this.load.image('sprBg', 'assets/sprBg.png');
        this.load.image('sprHeart', 'assets/heart.png');
        this.load.image('sprPower', 'assets/power.png');
        this.load.image('sprPause', 'assets/pause.png');
        this.load.image('sprParticle', 'assets/particle.png');

        // Audios
        this.load.audio('sndExplode', 'assets/sndExplode.wav');
        this.load.audio('sndLaser', 'assets/sndLaser.wav');
    }

    create() {
        // Animations
        this.anims.create({
            key: 'sprPlayer',
            frames: this.anims.generateFrameNumbers('sprPlayer'),
            frameRate: 20,
            repeat: -1
        });
        this.anims.create({
            key: 'sprEnemy',
            frames: this.anims.generateFrameNumbers('sprEnemy'),
            frameRate: 20,
            repeat: -1
        });
        this.anims.create({
            key: 'sprExplosion',
            frames: this.anims.generateFrameNumbers('sprExplosion'),
            frameRate: 20,
            repeat: 0
        });

        // Sound Effects
        this.sfx = {
            explosion: this.sound.add('sndExplode'),
            laser: this.sound.add("sndLaser")
        };

        // Scrolling background
        this.backgrounds = [];
        for (let i = 0; i < 5; i++) {
            const bg = new ScrollingBackground(this, 'sprBg', i * 30);
            this.backgrounds.push(bg);
        }

        // Player
        this.player = new Player(
            this,
            this.game.config.width * 0.5,
            this.game.config.height * 0.8,
            'sprPlayer'
        );

        // Enemies
        this.enemiesGen = this.time.addEvent({
            delay: 1000,
            callback: function() {
                const enemy = new GunShip(
                    this,
                    Phaser.Math.Between(0, this.game.config.width),
                    0,
                    level
                );
                enemy.setScale(Phaser.Math.Between(10, 20) * 0.1);
                this.enemies.add(enemy);
            },
            callbackScope: this,
            loop: true
        });

        // Control keys
        this.keyW = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
        this.keyS = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
        this.keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
        this.keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
        this.keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        this.keyP = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.P);
        this.keyJ = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.J);

        // Groups
        this.enemies = this.add.group();
        this.enemyLasers = this.add.group();
        this.playerLasers = this.add.group();


        // Lives
        this.player.lives = [];
        for (let i = 0; i < 3; i += 1) {
            const heart = this.add.image(640 - 150 + (50 * i), 40, 'sprHeart');
            this.player.lives.push(heart);
        }


        // Collision
        this.physics.add.collider(this.playerLasers, this.enemies, (playerLaser, enemy) => {
                if (enemy) {
                    this.explodeParticle(enemy.x, enemy.y);
                    if (enemy.onDestroy !== undefined) {
                        enemy.onDestroy();
                    }
                    enemy.explode(true);
                    playerLaser.destroy();
                    score += 10;
                }
        });
        this.physics.add.overlap(this.player, this.enemies, (player, enemy) => {
			if (player.getData('lives') <= 1) {
                player.setData('lives', 0);
                player.lives[0].destroy();

          		if (!player.getData('isDead') && !enemy.getData('isDead')) {
          		  	player.explode(false);
          		  	player.onDestroy();
          		  	enemy.explode(true);
                    this.explodeParticle(enemy.x, enemy.y);
          		}
			}
            else {
                player.setData('lives', player.getData('lives') - 1);
                enemy.explode(true);
                player.lives.pop().destroy();
            }
        });
        this.physics.add.overlap(this.player, this.enemyLasers, (player, laser) => {
			if (player.getData('lives') <= 1) {
                player.setData('lives', 0);
                player.lives[0].destroy();

          	    if (!player.getData('isDead') && !laser.getData('isDead')) {
          	      	player.explode(false);
          	      	player.onDestroy();
          	      	laser.destroy();
                    this.explodeParticle(player.x, player.y);
          	    }
            }
            else {
                player.setData('lives', player.getData('lives') - 1);
                laser.destroy();
                player.lives.pop().destroy();
            }
        });
        this.physics.add.overlap(this.enemyLasers, this.playerLasers, function(enemyLaser, playerLaser) {
            if (!playerLaser.getData('isDead') && !enemyLaser.getData('isDead')) {
                playerLaser.explode(true);
                enemyLaser.explode(true);
            }
        });

        // Score text
        this.scoreText = this.add.text(16, 16, 'Score: 0', {
            fontFamily: 'Arial',
            fontSize: 32,
            fontStyle: 'bold',
            color: '#f4d942',
            align: 'left'
        });

        // Level Text
        this.levelText = this.add.text(240, 16, 'Level 1', {
            fontFamily: 'Arial',
            fontSize: 32,
            fontStyle: 'bold',
            color: '#f4d942',
            align: 'center'
        });

        // Ultimate skill
        this.powerTime = this.time.addEvent({
            delay: 5000,
            callback: () => {
                this.power = this.add.image(390, 40, 'sprPower');
                this.canPower = true;
                this.powerTime.paused = true;
            },
            callbackScope: this,
            loop: true,
        });

        // Tween
        this.playerTween = this.tweens.add({
            targets: this.player,
            scaleX: 5,
            scaleY: 5,
            ease: 'Linear',
            duration: 500,
            yoyo: true,
            loop: 0
        });
        this.playerTween.pause();

        // Pause button
        this.btnPause = this.add.sprite(440, 40, 'sprPause');

        this.btnPause.setInteractive();

        this.btnPause.on('pointerdown', function() {
            this.scene.pause();
            this.scene.run('ScenePause');
        }, this);

        score = 0;
        level = 1;
    }

    update() {
        // Player controls
        if (!this.player.getData('isDead')) {
            this.player.update();

            if (this.keyW.isDown) {
                this.player.moveUp();
            }
            else if (this.keyS.isDown) {
                this.player.moveDown();
            }

            if (this.keyA.isDown) {
                this.player.moveLeft();
            }
            else if (this.keyD.isDown) {
                this.player.moveRight();
            }

            if (this.keySpace.isDown) {
                this.player.setData('isShooting', true);
            }
            else {
                this.player.setData('timerShootTick', this.player.getData('timerShootDelay') - 1);
                this.player.setData('isShooting', false);
            }
        }

        // Enemies, enemyLasers, and playerLasers
        this.arrayUpdate(this.enemies);
        this.arrayUpdate(this.enemyLasers);
        this.arrayUpdate(this.playerLasers);

        // Scrolling background
        for (let bg of this.backgrounds) {
            bg.update();
        }

        // Score text
        this.scoreText.setText('Score: ' + score);

        // Level text
        this.levelText.setText('Level ' + level);

        // Level controls
        if (score >= 50 && score < 100) {
            level = 2;
            this.enemiesGen.delay = 400;
        }
        if (score >= 100) {
            level = 3;
            this.enemiesGen.delay = 100;
        }

        // Ultimate skill
        if (this.keyJ.isDown && this.canPower) {
            this.ultimateSkill(this.enemies);
            this.ultimateSkill(this.enemyLasers);
            this.ultimateSkill(this.playerLasers);

            this.power.destroy();
            this.canPower = false;
            this.powerTime.paused = false;
            this.playerTween.play();
        }
    }

    // Destroy when out of display to reduce memory usage
    arrayUpdate(parent) {
        for (let child of parent.getChildren()) {
          child.update(level);

          if (child.x < -child.displayWidth ||
            child.x > this.game.config.width + child.displayWidth ||
            child.y < -child.displayHeight * 4 ||
            child.y > this.game.config.height + child.displayHeight) {
            if (child) {
              if (child.onDestroy !== undefined) {
                child.onDestroy();
              }
              child.destroy();
            }
          }
        }
    }

    ultimateSkill(parent) {
        for (let child of parent.getChildren()) {
            if (child) {
                this.explodeParticle(child.x, child.y);

                if (child.onDestroy !== undefined) {
                  child.onDestroy();
                }
                child.destroy();
            }
        }
    }

    // Particle
    explodeParticle(posX, posY) {
        const particles = this.add.particles('sprParticle');

        particles.createEmitter({
            x: posX,
            y: posY,
            angle: { min: 0, max: 360 },
            speedX: { min: -150, max: 150 },
            speedY: { min: -150, max: 150 },
            gravityX: 0,
            gravityY: 0,
            scale: { start: 2, end: 0 },
            maxParticles: 20,
            lifespan: 800,
            on: true
        });
    }
}

// Score
let score = 0;

// Level
let level = 1;
